#pragma once

/* CLIENT FOR EPUCK ROBOT*/

#include <arpa/inet.h>
#include <cstdio> // Pour les Sprintf
#include <fstream>
#include <iostream>
#include <netinet/in.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include <opencv2/opencv.hpp>
#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctime>
#include <unistd.h>
#include <signal.h>

#define INVALID_SOCKET -1
#define SOCKET_ERROR -1

using namespace std;
using namespace cv;

/**********************************/
/**** Definition of variables ****/
/**********************************/
typedef int SOCKET;
SOCKET CameraSocket, CommandSendingSocket, SensorReceivingSocket;



// Camera variables
bool CameraActive = false;
struct imageData {
    int id_img;
    int id_block;
    unsigned long date;
    unsigned char msg[230400];
} imgData;

// Robot Parameters
struct robotParameters {
    const float theta[10] = {-0.2268, -0.8371, -1.5708, -2.2391, 2.2391,
                             1.5708,  0.8371,  0.2268,  0.35,    -0.35};
    const float robRadius = 0.033;
    const float wheel_radius = 0.0205;
    const float axisLength = 0.026;
} rp;

/* Variables thread principal */
char Capteurs[100]; // Buffer de reception des capteurs
int EncodeurG, EncodeurD, oldEncodeurG, oldEncodeurD;
int CapteurProx[10];
bool stop_threads = false;

float xA, yA, xB, yB;
/* Variables sauvegarde */
std::string images_log_folder;
FILE *x_A, *y_A, *x_B, *y_B;
FILE *eg, *ed, *p0, *p1, *p2, *p3, *p4, *p5, *p6, *p7, *p8, *p9;

// Valeurs des encodeurs, vitesse et capteurs de proximite
float xRobot, yRobot, thetaRob;

// General functions
void sigint_handler(int signal);

inline int sign(float val);

// Functions for opening sockets
void InitSocketOpening(const std::string& epuck_ip);

void OpenCameraSocket();

void OpenSensorReceivingSocket();

/* Socket EnvoieCommandes */
void OpenCommandSendingSocket(const std::string& epuck_ip);

/**** Fonction de fermeture de socket ****/
void CloseSocket(int NOM_SOCKET);

void ProcessInfraRed(); 

void SendMotorAndLEDCommandToRobot(const char MotorCmd[15]);

void ReceptionCapteur();

void DecoupeReception();

/**** fonction initialisation Camera ****/
void InitCamera(const std::string& epuck_ip);

/*****************/
/**** Threads ****/
/*****************/

/**** Thread for receiving camera data ****/
void* CameraReceptionThread(void* arg);

// SPECIFIC FUNCTIONS
void SaveData(const string & data_folder);

/**** Send commands to the wheel motors for 10 seconds****/
void SetWheelCommands(struct timeval startTime, const int& speed_L,
                      const int& speed_R, char MotorCmd[15]);

/**** Projection mesures infrarouges dans repere robot - fonction specifique de
 * ce programme****/
void ProjectionInfrarougesDansRepereRobotEtSauvetage(const string & data_folder);

std::string createLogFolder();
