cmake_minimum_required(VERSION 3.2)

# Project description
project(
    epuck-client
    VERSION 1.0
    DESCRIPTION "A C++ client to communicate with the E-Puck robot"
    LANGUAGES CXX)

# Declare the dependencies of this project
find_package(OpenCV REQUIRED COMPONENTS opencv_core opencv_highgui opencv_imgcodecs opencv_imgproc)
find_package(Threads)

# Process the src and apps directories
add_subdirectory(src)
add_subdirectory(apps)

# Force the generation of a compile_commands.json file to provide autocompletion for IDEs
set(CMAKE_EXPORT_COMPILE_COMMANDS TRUE CACHE BOOL "" FORCE)
