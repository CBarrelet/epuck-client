#include <epuck.h>
#include <controlFunctions.h>


/*****************************/
/**** Main Program ***********/
/*****************************/

void IncorrectArguments(const int& argc);

int main(int argc, char** argv) {

    if (argc != 2) {
        IncorrectArguments(argc);
    }
    std::string nameOfReadFolder = argv[1];
    int cnt = 2;
    RobPose prevPoseFromEnc, curPoseFromEnc, prevPoseFromVis, curPoseFromVis;
    std::vector<int> encL, encR;
    encL = LoadEncoderLogs(nameOfReadFolder + "data/eg.txt");
    encR = LoadEncoderLogs(nameOfReadFolder + "data/ed.txt");
    std::string nameOfWriteFolder = createLogFolder();
    while(true){
        printf("\033[1;36m");//write in bold cyan
        printf("\nSTART ITERATION %d \n", cnt);
        printf("\033[0m");//reset color

        Mat colImgFromDisk = LoadAndShowImageFromDisk(nameOfReadFolder, cnt);
        Point baryc = ProcessImageToGetBarycenter(colImgFromDisk);

        if(cnt ==2) {
            prevPoseFromEnc.setPose(.3, .15, M_PI);
            prevPoseFromVis.setPose(.15, .10, M_PI);
        }
        curPoseFromEnc = GetCurrPoseFromEncoders(prevPoseFromEnc, encL[cnt-1], encR[cnt-1], encL[cnt-2], encR[cnt-2], nameOfWriteFolder);
        prevPoseFromEnc = curPoseFromEnc;

        curPoseFromVis = prevPoseFromVis;//TODO

        DrawMapWithRobot(curPoseFromEnc, curPoseFromVis);
        waitKey(0);
        cnt++;
    }
    return 0;
}


void IncorrectArguments(const int& argc) {
    printf("There are %d arguments instead of 1\n", argc - 1);
    printf("The argument should be the path to the folder containing the images to be processed \n");
    exit(0);
}
